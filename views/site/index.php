<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'PDF Generator';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome!</h1>

        <p class="lead">To generate PDFs with unique QR codes please click the button below.</p>

        <p>
            <?= Html::a('Generate', ['/site/generate'], ['class' => 'btn btn-lg btn-success']) ?>
    </div>
    <div class="body-content"></div>
</div>
