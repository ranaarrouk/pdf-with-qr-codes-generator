<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Generate';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-generate">
    <h1><?= Html::encode($this->title) ?></h1>
    <p>

    </p>

    <?php if (isset($errors)) {
        echo '    <div class="alert alert-danger"><h5>Please check: </h5>';
        foreach ($errors as $error) {
            echo $error[0];
            echo '<br>';
        };
    } else if (isset($PDFNames) && count($PDFNames) > 0) {
        echo '    <div class="alert alert-success"><h5>Generated PDFs: </h5>';
        foreach ($PDFNames as $name) {
            echo HTML::a($name, $name, ['target' => '_blank']);
            echo '<br><br>';
        };
    }
    echo '<br><br></div>';
    ?>
    <div class="row">

        <div class="col-lg-12">

            <?php ActiveForm::begin(['id' => 'generate-form']); ?>

            <div class="inputs-div">
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-3">
                        <?= Html::input('number', 'numbers[]', null, ['class' => 'form-control', 'placeholder' => 'Number of QR codes']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::dropDownList('colors[]', null, ['blue' => 'blue', 'red' => 'red', 'green' => 'green'], ['class' => 'form-control', 'placeholder' => 'QR color']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::input('prefix', 'prefixes[]', null, ['class' => 'form-control', 'placeholder' => 'QR prefix']) ?>
                    </div>
                    <div class="col-md-3">
                        <?= Html::a('Add more', null, ['id' => 'add-button', 'class' => 'btn btn-success add-button']) ?>
                    </div>
                </div>
            </div>

            <div class="row row-to-clone hidden" style="margin-bottom: 10px">
                <div class="col-md-3">
                    <?= Html::input('number', 'numbers[]', null, ['class' => 'form-control', 'placeholder' => 'Number of QR codes', 'disabled' => 'disabled']) ?>
                </div>
                <div class="col-md-3">
                    <?= Html::dropDownList('colors[]', null, ['blue' => 'blue', 'red' => 'red', 'green' => 'green'], ['class' => 'form-control', 'placeholder' => 'QR color', 'disabled' => 'disabled']) ?>
                </div>
                <div class="col-md-3">
                    <?= Html::input('text', 'prefixes[]', null, ['class' => 'form-control', 'placeholder' => 'QR prefix', 'disabled' => 'disabled']) ?>
                </div>
            </div>

            <div id="submit-button" class="form-group" style="margin-top: 15px">
                <?= Html::submitButton('Generate', ['class' => 'btn btn-primary', 'name' => 'generate-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<script>
    window.onload = function () {
        $("#add-button").on("click", function (e) {
            let div = $('.row-to-clone').clone();
            div.removeClass('hidden').removeClass('row-to-clone');
            div.find('input, select').attr('disabled', false);
            $('.inputs-div').append(div);
        });
    }
</script>