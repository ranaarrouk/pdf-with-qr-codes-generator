<?php

namespace app\models;

use app\helpers\PdfHelper;
use app\helpers\QRCodeHelper;
use yii\base\Model;

/**
 * GenerateForm is the model behind the generate form.
 */
class GenerateForm extends Model
{
    public $number;
    public $color;
    public $prefix;


    public function __construct($number, $color, $prefix)
    {
        $this->number = $number;
        $this->color = $color;
        $this->prefix = $prefix;
        parent::__construct();
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // numbers, email, subject and body are required
            [['number', 'prefix'], 'required'],
            ['number', 'number', 'min' => 1],
        ];
    }

    public function generate()
    {
        $params = QRCodeHelper::getColorParams($this->color);

        $content = $fileName = '';

        for ($i = 1; $i <= $this->number; $i++) {
            try {
                $qrCode = QRCodeHelper::newCode($this->prefix, $i, 300, 5, $params);
                $content .= '<img src="' . $qrCode->writeDataUri() . '" style="margin-bottom: 40px;margin-right: 20px">';

                $pdf = PdfHelper::newFile($content, 'Rana Arrouk Test', 18);

                $pdf->filename = 'assets/myPDFs/PDF_' . $this->number . $this->color . $this->prefix . date('Y-m-d') . '.pdf';

                $fileName = $pdf->filename;
                $pdf->render();

            } catch (\Exception $exception) {
                \Yii::error("Something went wrong: " . $exception->getMessage());
            }
        }
        return $fileName;
    }
}
