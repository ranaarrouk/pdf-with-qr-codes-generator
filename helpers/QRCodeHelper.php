<?php

namespace app\helpers;

use Da\QrCode\QrCode;

class QRCodeHelper
{
    public static function newCode($prefix, $index, $size, $margin, $color = [])
    {
        return (new QrCode($prefix . uniqid($index)))
            ->setSize($size)
            ->setMargin($margin)
            ->useForegroundColor($color[0], $color[1], $color[2]);
    }

    public static function getColorParams($color)
    {
        switch ($color)
        {
            case 'red': return [168, 34, 7];
            case 'blue': return [8, 115, 209];
            case 'green': return [19, 148, 66];
            default: return [8, 115, 209];
        }
    }

}